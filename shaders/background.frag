#version 330

in vec4 gl_FragCoord;

out vec4 frag_color;

uniform ivec2 window_size;

const float cell_size = 0.1;
const float line_size = 1.5;

uniform float animation_state; // 0..1_0..1
const float PI = 3.141592653589793;

void main() {
	vec3 color = vec3(0.1);
	float cycling_range = (sin(animation_state * 2*PI) + 1)/2; // 0..1_1..0

	float small_side = min(window_size.x, window_size.y);
	float big_side = max(window_size.x, window_size.y);
	vec2 coords = gl_FragCoord.xy / small_side;

	float spacing = (big_side - small_side)/2 / small_side;
	bool is_wider = window_size.x > window_size.y;
	coords.x -= spacing * float(is_wider);
	coords.y -= spacing * float(!is_wider);

	vec2 dist = 16 * vec2(
			distance(mod(coords.x, cell_size), cell_size / 2.0),
			distance(mod(coords.y, cell_size), cell_size / 2.0));

	float actual_line_size = (cycling_range/4 + 1) * max(line_size, 1.3);
	if (dist.x > 1/actual_line_size || dist.y > 1/actual_line_size) color = vec3(0.13);

	frag_color = vec4(color, 1.0);
}
