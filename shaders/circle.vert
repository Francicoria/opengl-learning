#version 330
layout (location = 0) in vec2 vert_pos;

out vec2 fwd_vert_pos;

uniform vec2 params;
uniform float radius;

void main() {
	vec2 pos = vert_pos - radius;
	fwd_vert_pos = pos;
	gl_Position = vec4(pos + params, 0.0, 1.0);
}
