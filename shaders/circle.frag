#version 330

in vec2 fwd_vert_pos;
in vec4 gl_FragCoord;

out vec4 frag_color;

uniform float radius;

void main() {
	vec2 coords = fwd_vert_pos / radius;

	float alpha = 1.0;
	if (distance(coords, vec2(0)) > 1) alpha = 0.0;

	frag_color = vec4(0.0, 0.0, 1.0, alpha);
}
