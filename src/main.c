// @todo Log confirmation when successfully doing things
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define SHADERS_FOLDER "shaders"

#define INITIAL_WINDOW_WIDTH  (600)
#define INITIAL_WINDOW_HEIGHT (600)

#define BACKGROUND_ANIMATION_SPEED (0.3)

#define PLAYER_WIDTH (0.4)
#define PLAYER_HEIGHT (0.05)
#define PLAYER_VELOCITY_DELTA (0.5)
#define PLAYER_MAX_VELOCITY (0.5)

#define BALL_RADIUS (0.05)
#define BALL_VELOCITY_DELTA (0.005)
#define BALL_MAX_VELOCITY (0.010)

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define clamp(x, a, b) max((a), min((x), (b)))

#define report_fmt(level, lib, s, ...) fprintf(stderr, #level " [" #lib "]: " s "\n", __VA_ARGS__)
#define report(level, lib, s) fprintf(stderr, #level " [" #lib "]: " s "\n")

#define report_glfw_error(text) do { \
	const char *reason; \
	assert(glfwGetError(&reason) != GLFW_NO_ERROR); \
	report_fmt(ERROR, GLFW, text ": %s\n", reason); \
} while (0)

bool wireframe_mode = false;

void framebuffer_size_callback(GLFWwindow *window, int new_width, int new_height) {
	(void)window;
	glViewport(0, 0, new_width, new_height);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	(void)scancode;
	(void)action;
	(void)mods;

	switch (key) {
	case GLFW_KEY_Q: if (action == GLFW_RELEASE) {
		glfwSetWindowShouldClose(window, true);
	} break;
	case GLFW_KEY_F1: if (action == GLFW_RELEASE) {
		glPolygonMode(GL_FRONT_AND_BACK, wireframe_mode ? GL_FILL : GL_LINE);
		wireframe_mode = !wireframe_mode;
	} break;
	default: {}
	}
}

typedef struct {
	const char *str;
	size_t len;
} StringV;

static StringV get_entire_file_content(const char *filepath) {
	FILE *f = fopen(filepath, "rb");
	if (!f) {
		report_fmt(ERROR, SYSTEM, "couldn't open file \"%s\" for reading", filepath);
		goto err;
	}
	if (fseek(f, 0, SEEK_END)) goto err;

	long len = ftell(f);
	if (len <= 0) goto err;

	if (fseek(f, 0, SEEK_SET)) goto err;

	char *content = (char *)malloc(len);
	if (!content) goto err;

	if (fread(content, 1, len, f) < (size_t)len) {
		report_fmt(ERROR, SYSTEM, "couldn't read from file \"%s\"", filepath);
		goto err;
	}

	return (StringV){
		.str = content,
			.len = len,
	};

err:
	if (f) fclose(f);
	return (StringV){0};
}

typedef enum {
	VERTEX_SHADER,
	FRAGMENT_SHADER,
} ShaderType;

static GLuint compile_shader(ShaderType shader_type, const char *shader_filepath) {
	const char *stype_text =       (shader_type == VERTEX_SHADER) ? "vertex"           : "fragment";
	GLuint shader = glCreateShader((shader_type == VERTEX_SHADER) ? GL_VERTEX_SHADER   : GL_FRAGMENT_SHADER);
	if (!shader) {
		report_fmt(ERROR, GL, "couldn't create %s shader", stype_text);
		return 0;
	}

	StringV content = get_entire_file_content(shader_filepath);
	if (!content.str) {
		return 0;
	}

	glShaderSource(shader, 1, &content.str, (GLint *)&content.len);
	glCompileShader(shader);

	free((void *)content.str);

	GLint shader_compiled = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_compiled);
	{ // Log info and possible errors
		GLchar log_string[1024];
		GLsizei log_string_length = 0;
		glGetShaderInfoLog(shader, sizeof(log_string), &log_string_length, log_string);
		fprintf(stderr, "%.*s", log_string_length, log_string);
	}
	if (shader_compiled == GL_FALSE) {
		report_fmt(ERROR, GL, "couldn't compile \"%s\" %s shader", shader_filepath, stype_text);
		return 0;
	}

	report_fmt(INFO, GL, "\"%s\" %s shader compiled successfully", shader_filepath, stype_text);

	return shader;
}

static GLuint compile_program(GLuint vert_shader, GLuint frag_shader) {
	GLuint program = glCreateProgram();
	if (!program) {
		report(ERROR, GL, "couldn't create program");
		return 0;
	}
	glAttachShader(program, vert_shader);
	glAttachShader(program, frag_shader);

	glLinkProgram(program);
	GLint program_linked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, &program_linked);
	{ // Log info and possible errors
		GLchar log_string[1024];
		GLsizei log_string_length = 0;
		glGetProgramInfoLog(program, sizeof(log_string), &log_string_length, log_string);
		fprintf(stderr, "%.*s", log_string_length, log_string);
	}
	if (program_linked == GL_FALSE) {
		report(ERROR, GL, "couldn't link program");
		return 0;
	}

	report(INFO, GL, "program linked successfully");

	return program;
}

typedef struct {
	float x, y;
} Vec2;

typedef enum {
	COLLISION_NONE = 0,
	COLLISION_HORIZ_SIDES,
	COLLISION_VERT_SIDES,
} Collision_Rect;

static Collision_Rect ball_rect_collide(Vec2 ball, float ball_radius, Vec2 rect, float rect_width, float rect_height) {
	Vec2 nearest_point = {0};
	nearest_point.x = clamp(ball.x, rect.x, rect.x + rect_width);
	nearest_point.y = clamp(ball.y, rect.y, rect.y + rect_height);

	float x = nearest_point.x - ball.x;
	float y = nearest_point.y - ball.y;
	if (x*x + y*y <= ball_radius*ball_radius) {
		if (nearest_point.x == rect.x || nearest_point.x == rect.x + rect_width) return COLLISION_HORIZ_SIDES;
		return COLLISION_VERT_SIDES;
	}

	return COLLISION_NONE;
}

int main(void) {
	if (!glfwInit()) {
		report_glfw_error("couldn't initialize GLFW");
		goto cleanup_err;
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow *window = glfwCreateWindow(
			INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT,
			"My cool window",
			NULL, NULL);
	if (!window) {
		report_glfw_error("couldn't create window");
		goto cleanup_err;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGL()) {
		report(ERROR, GLAD, "couldn't initialize glad");
		return 1;
	}

	GLuint simple_vert_shader = compile_shader(VERTEX_SHADER, SHADERS_FOLDER"/simple.vert");
	if (!simple_vert_shader) goto cleanup_err;

	GLuint rect_vert_shader = compile_shader(VERTEX_SHADER, SHADERS_FOLDER"/rect.vert");
	if (!rect_vert_shader) goto cleanup_err;

	GLuint circle_vert_shader = compile_shader(VERTEX_SHADER, SHADERS_FOLDER"/circle.vert");
	if (!circle_vert_shader) goto cleanup_err;

	GLuint green_frag_shader = compile_shader(FRAGMENT_SHADER, SHADERS_FOLDER"/green.frag");
	if (!green_frag_shader) goto cleanup_err;

	GLuint circle_frag_shader = compile_shader(FRAGMENT_SHADER, SHADERS_FOLDER"/circle.frag");
	if (!circle_frag_shader) goto cleanup_err;

	GLuint rect_frag_shader = compile_shader(FRAGMENT_SHADER, SHADERS_FOLDER"/rect.frag");
	if (!rect_frag_shader) goto cleanup_err;

	GLuint bg_frag_shader = compile_shader(FRAGMENT_SHADER, SHADERS_FOLDER"/background.frag");
	if (!bg_frag_shader) goto cleanup_err;

	GLuint green_program = compile_program(simple_vert_shader, green_frag_shader);
	if (!green_program) goto cleanup_err;

	GLuint bg_program = compile_program(simple_vert_shader, bg_frag_shader);
	if (!bg_program) goto cleanup_err;
	GLint bg_window_size_pos = glGetUniformLocation(bg_program, "window_size");
	GLint bg_animation_state_pos = glGetUniformLocation(bg_program, "animation_state");

	GLuint circle_program = compile_program(circle_vert_shader, circle_frag_shader);
	if (!circle_program) goto cleanup_err;
	GLint circle_radius_pos = glGetUniformLocation(circle_program, "radius");
	GLint circle_params_pos = glGetUniformLocation(circle_program, "params");

	GLuint rect_program = compile_program(rect_vert_shader, rect_frag_shader);
	GLint rect_window_size_pos = glGetUniformLocation(rect_program, "window_size");
	GLint rect_params_pos = glGetUniformLocation(rect_program, "params");

	if (!rect_program) goto cleanup_err;

	glViewport(0, 0, INITIAL_WINDOW_WIDTH, INITIAL_WINDOW_HEIGHT);
	if (!glfwSetFramebufferSizeCallback(window, framebuffer_size_callback) &&
			glfwGetError(NULL) != GLFW_NO_ERROR) {
		report_glfw_error("failed to set framebuffer size callback");
		goto cleanup_err;
	}

	if (!glfwSetKeyCallback(window, key_callback) &&
			glfwGetError(NULL) != GLFW_NO_ERROR) {
		report_glfw_error("failed to set key callback");
		goto cleanup_err;
	}

#define NUMBER_OF_VAOS (4)
	GLuint vaos[NUMBER_OF_VAOS];
	glGenVertexArrays(NUMBER_OF_VAOS, vaos);

	GLuint bg_vao = vaos[0];
	glBindVertexArray(bg_vao);
	{
		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		const float verts[] = {
			-1.0, -1.0,
			-1.0, 1.0,
			1.0, -1.0,

			1.0, 1.0,
			-1.0, 1.0,
			1.0, -1.0,
		};
		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(
				0, // in the vertex shader we specified 'pos' at location 0
				2, // 'pos' is a vec2
				GL_FLOAT,
				GL_FALSE, // don't normalize my data
				2 * sizeof(float), // stride
				NULL);
	}

	GLuint green_triangle_vao = vaos[1];
	glBindVertexArray(green_triangle_vao);
	{
		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		const float verts[] = {
			-0.2, 0.8,
			-0.8, 0.4,
			-0.3, 0.1,
		};
		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(
				0, // in the vertex shader we specified 'pos' at location 0
				2, // 'pos' is a vec2
				GL_FLOAT,
				GL_FALSE, // don't normalize my data
				2 * sizeof(float), // stride
				NULL);
	}

	GLuint player_model_vao = vaos[2];
	glBindVertexArray(player_model_vao);
	{
		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		const float verts[] = {
			0.0,          0.0,
			0.0,          PLAYER_HEIGHT,
			PLAYER_WIDTH, 0.0,

			PLAYER_WIDTH, PLAYER_HEIGHT,
			0.0,          PLAYER_HEIGHT,
			PLAYER_WIDTH, 0.0,
		};

		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(
				0, // in the vertex shader we specified 'pos' at location 0
				2, // 'pos' is a vec2
				GL_FLOAT,
				GL_FALSE, // don't normalize my data
				2 * sizeof(float), // stride
				NULL);
	}

	GLuint ball_model_vao = vaos[3];
	glBindVertexArray(ball_model_vao);
	{
		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		const float verts[] = {
			0.0,             0.0,
			0.0,             2 * BALL_RADIUS,
			2 * BALL_RADIUS, 0.0,

			0.0,             2 * BALL_RADIUS,
			2 * BALL_RADIUS, 0.0,
			2 * BALL_RADIUS, 2 * BALL_RADIUS,
		};

		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, vbo);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(
				0, // in the vertex shader we specified 'pos' at location 0
				2, // 'pos' is a vec2
				GL_FLOAT,
				GL_FALSE, // don't normalize my data
				2 * sizeof(float), // stride
				NULL);
	}

	// enable alpha blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	float bg_animation_state = 0;

	Vec2 player_pos = {0, -0.7};
	Vec2 player_vel = {0, 0};

	Vec2 ball_pos = {0.4, 0};
	Vec2 ball_vel = {0, 0};

	struct timespec previous_frame;
	timespec_get(&previous_frame, TIME_UTC);
	struct timespec current_frame;

	while (!glfwWindowShouldClose(window)) {
		timespec_get(&current_frame, TIME_UTC);
		float diffsec = difftime(current_frame.tv_sec, previous_frame.tv_sec);
		assert(diffsec >= 0);
		float dt = (current_frame.tv_nsec - previous_frame.tv_nsec) * 1e-9 + diffsec;
		previous_frame = current_frame;

		int w = 0;
		int h = 0;
		glfwGetWindowSize(window, &w, &h);
		bg_animation_state += BACKGROUND_ANIMATION_SPEED * dt;
		bg_animation_state = fmod(bg_animation_state, 1.0);

		{ // handle player movement
			// slow down if we're moving
			if (player_vel.x != 0) player_vel.x *= 0.7;

			if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) player_vel.x += PLAYER_VELOCITY_DELTA * dt;
			if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) player_vel.x -= PLAYER_VELOCITY_DELTA * dt;

			int sign_x = player_vel.x < 0.0 ? -1 : +1;
			// @todo check negative velocity too
			if (fabsf(player_vel.x) > PLAYER_MAX_VELOCITY) player_vel.x = PLAYER_MAX_VELOCITY * sign_x;

			float x0 = player_pos.x;
			float x1 = player_pos.x + PLAYER_WIDTH;
			if (x1 + player_vel.x > 1.0) {
				player_vel.x = 1.0 - x1;
			}
			if (x0 + player_vel.x < -1.0) {
				player_vel.x = -1.0 - x0;
			}

			player_pos.x += player_vel.x;
		}

		{
			int sign_x = ball_vel.x < 0.0 ? -1 : +1;
			int sign_y = ball_vel.y < 0.0 ? -1 : +1;

			ball_vel.x += BALL_VELOCITY_DELTA * dt * sign_x;
			ball_vel.y += BALL_VELOCITY_DELTA * dt * sign_y;

			// terminal velocity check
			if (fabsf(ball_vel.x) > BALL_MAX_VELOCITY) ball_vel.x = BALL_MAX_VELOCITY * sign_x;
			if (fabsf(ball_vel.y) > BALL_MAX_VELOCITY) ball_vel.y = BALL_MAX_VELOCITY * sign_y;

			// screen border collision
			if (ball_pos.x + BALL_RADIUS > 1.0 || ball_pos.x - BALL_RADIUS < -1.0) ball_vel.x *= -1;
			if (ball_pos.y + BALL_RADIUS > 1.0 || ball_pos.y - BALL_RADIUS < -1.0) ball_vel.y *= -1;

			switch (ball_rect_collide(ball_pos, BALL_RADIUS, player_pos, PLAYER_WIDTH, PLAYER_HEIGHT)) {
			case COLLISION_VERT_SIDES:  ball_vel.y *= -1; break;
			case COLLISION_HORIZ_SIDES: ball_vel.x *= -1; break;
			default: {}
			}

			ball_pos.x += ball_vel.x;
			ball_pos.y += ball_vel.y;
		}

		glClearColor(0.1, 0.1, 0.1, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(bg_program);
		glUniform2i(bg_window_size_pos, w, h);
		glUniform1f(bg_animation_state_pos, bg_animation_state);
		glBindVertexArray(bg_vao);
		glDrawArrays(GL_TRIANGLES, 0, 2*3);

		glUseProgram(green_program);
		glBindVertexArray(green_triangle_vao);
		glDrawArrays(GL_TRIANGLES, 0, 3);

		glUseProgram(rect_program);
		glUniform2i(rect_window_size_pos, w, h);
		glUniform2f(rect_params_pos, player_pos.x, player_pos.y);
		glBindVertexArray(player_model_vao);
		glDrawArrays(GL_TRIANGLES, 0, 2*3);

		glUseProgram(circle_program);
		glUniform2f(circle_params_pos, ball_pos.x, ball_pos.y);
		glUniform1f(circle_radius_pos, BALL_RADIUS);
		glBindVertexArray(ball_model_vao);
		glDrawArrays(GL_TRIANGLES, 0, 2*3);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;

cleanup_err:
	glfwTerminate();
	return 1;
}
