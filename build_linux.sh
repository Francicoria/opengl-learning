#!/bin/sh

CC="gcc"
CFLAGS="-Wall -Wextra -Wpedantic -ggdb"
OUT="main"
SRC="glad.o src/main.c"

set -xe

${CC} -Iinclude -c -o glad.o src/glad.c
${CC} ${CFLAGS} -Iinclude -Llib -o ${OUT} ${SRC} -lglfw3 -lm -lX11
